/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

define([
    'jquery'
], function (
    $
) {
    let outOfStock = $.mage.__('Out of Stock');
    let availableQty = $.mage.__('Available Qty: %1');

    let getPhysicalStock = (url) => {
        $.ajax({

            url: url,
            type: 'GET',
            data: {
                format: 'json'
            },
            dataType: 'json',
            success: function (data) {
                let stockQty = parseInt(data[0].quantity);
                let qty = stockQty > 0 ? availableQty.replace('%1', stockQty) : outOfStock;
                $('#physical-stock-quantity').html(qty)
            },
            error: function () {
                $('#physical-stock-quantity').html(outOfStock);
            }
        });
    }

    return function (config) {
        getPhysicalStock(config.url);
    }
});
