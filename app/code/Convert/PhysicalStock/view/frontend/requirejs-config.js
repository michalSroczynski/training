/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

let config = {
    map: {
        '*': {
            physicalStock: 'Convert_PhysicalStock/js/physicalStock'
        }
    }
};
