<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\PhysicalStock\Model\ResourceModel\PhysicalStock;

use Convert\PhysicalStock\Model\PhysicalStock;
use Convert\PhysicalStock\Model\ResourceModel\PhysicalStock as PhysicalStockResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(PhysicalStock::class, PhysicalStockResourceModel::class);
    }
}
