<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\PhysicalStock\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class PhysicalStock extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('convert_physical_stock', 'entity_id');
    }
}
