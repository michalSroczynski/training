<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\PhysicalStock\Model;

use Magento\Framework\Model\AbstractModel;
use Convert\PhysicalStock\Model\ResourceModel\PhysicalStock as PhysicalStockResourceModel;

class PhysicalStock extends AbstractModel
{
    /** @var string */
    const CACHE_TAG = 'convert_physicalstock_physicalstock';

    public function _construct()
    {
        $this->_init(PhysicalStockResourceModel::class);
    }
}
