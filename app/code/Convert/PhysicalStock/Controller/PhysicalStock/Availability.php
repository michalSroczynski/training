<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\PhysicalStock\Controller\PhysicalStock;

use Convert\PhysicalStock\Helper\Data;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Webapi\Exception;
use Magento\Store\Model\StoreManagerInterface;

class Availability implements HttpGetActionInterface
{
    /** @var Data */
    protected $helperData;

    /** @var JsonFactory */
    protected $jsonFactory;

    /** @var StoreManagerInterface */
    protected $storeManager;

    /** @var Http */
    protected $request;

    public function __construct(
        Data $helperData,
        JsonFactory $jsonFactory,
        StoreManagerInterface $storeManager,
        Http $request
    ) {
        $this->helperData = $helperData;
        $this->jsonFactory = $jsonFactory;
        $this->storeManager = $storeManager;
        $this->request = $request;
    }

    /**
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $result = $this->jsonFactory->create();

        try {
            $store = $this->storeManager->getStore();

            if (!$this->helperData->isPhysicalStockEnabled($store)) {
                return $this->getExceptionResponse(
                    $result,
                    Exception::HTTP_INTERNAL_ERROR,
                    (string)__('Physical stock for store is not enabled.')
                );
            }

            $productId = (int)$this->request->getParam('product_id');

            $product = $this->helperData->getProductById($productId);
            $physicalStock = $this->helperData->getPhysicalStock($product, $store);
        } catch (\Exception $exception) {
            return $this->getExceptionResponse($result, Exception::HTTP_NOT_FOUND, $exception->getMessage());
        }

        return $result->setData($physicalStock->getData());
    }

    /**
     * @param Json $jsonFactory
     * @param int $responseCode
     * @param string $message
     * @return Json
     */
    private function getExceptionResponse(Json $jsonFactory, int $responseCode, string $message): Json
    {
        $jsonFactory->setHttpResponseCode($responseCode);
        $jsonFactory->setData(['error_message' => $message]);
        return $jsonFactory;
    }
}
