<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\PhysicalStock\Block\Product\View;

use Convert\PhysicalStock\Helper\Data;
use Convert\Training\Registry\CurrentProduct;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Store\Api\Data\StoreInterface;

class PhysicalStock extends Template
{
    /** @var CurrentProduct */
    protected $currentProduct;

    /** @var StoreInterface */
    protected $store;

    /** @var Data */
    protected $dataHelper;

    /**
     * NewAttribute constructor.
     * @param Template\Context $context
     * @param Data $dataHelper
     * @param array $data
     * @throws NoSuchEntityException
     */
    public function __construct(Template\Context $context, Data $dataHelper, array $data = [])
    {
        parent::__construct($context, $data);

        $this->currentProduct = $this->getData('product');
        $this->store = $context->getStoreManager()->getStore();
        $this->dataHelper = $dataHelper;
    }

    /**
     * @return bool
     */
    public function isPhysicalStockEnabled(): bool
    {
        return $this->dataHelper->isPhysicalStockEnabled($this->store);
    }

    /**
     * @return int
     */
    public function getCurrentProductId(): int
    {
        return (int)$this->currentProduct->get()->getId();
    }

    /**
     * @return string
     */
    public function _toHtml(): string
    {
        if (!$this->isPhysicalStockEnabled()) {
            return '';
        }

        return parent::_toHtml();
    }
}
