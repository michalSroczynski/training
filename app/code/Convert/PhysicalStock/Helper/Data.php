<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\PhysicalStock\Helper;

use Convert\PhysicalStock\Exception\PhysicalStockNotFoundException;
use Convert\PhysicalStock\Model\ResourceModel\PhysicalStock\Collection;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const PHYSICAL_STOCK_PATH = 'physical-stock-section/general/enable';

    /** @var Collection */
    protected $physicalStockCollection;

    /** @var ProductRepositoryInterface */
    protected $productRepository;

    public function __construct(
        Context $context,
        Collection $physicalStockCollection,
        ProductRepositoryInterface $productRepository
    ) {
        parent::__construct($context);
        $this->physicalStockCollection = $physicalStockCollection;
        $this->productRepository = $productRepository;
    }

    /**
     * @param StoreInterface $scopeCode
     * @return bool
     */
    public function isPhysicalStockEnabled(StoreInterface $scopeCode): bool
    {
        return (bool)$this->scopeConfig->getValue(
            self::PHYSICAL_STOCK_PATH,
            ScopeInterface::SCOPE_STORE,
            $scopeCode
        );
    }

    /**
     * @param ProductInterface $product
     * @param StoreInterface $store
     * @return Collection
     * @throws PhysicalStockNotFoundException
     */
    public function getPhysicalStock(ProductInterface $product, StoreInterface $store): Collection
    {
        $collection = $this->physicalStockCollection
            ->addFieldToFilter('catalog_product_id', $product->getId())
            ->addFieldToFilter('store_id', $store->getId());
        $physicalStock = $collection->loadData();

        if (!$physicalStock->getFirstItem()->getId()) {
            throw new PhysicalStockNotFoundException(
                __(
                    'Physical Stock not found for product ID: %1 and store ID: %2',
                    $product->getId(),
                    $store->getId()
                )
            );
        }

        return $physicalStock;
    }

    /**
     * @param int $productId
     * @return ProductInterface
     * @throws NoSuchEntityException
     */
    public function getProductById(int $productId): ProductInterface
    {
        return $this->productRepository->getById($productId);
    }
}
