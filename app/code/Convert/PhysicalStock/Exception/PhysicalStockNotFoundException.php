<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\PhysicalStock\Exception;

use Magento\Framework\Exception\NotFoundException;

class PhysicalStockNotFoundException extends NotFoundException
{
}
