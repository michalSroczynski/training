<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Blog\Block\Adminhtml\Edit;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class SaveButton implements ButtonProviderInterface
{
    /** @var Context */
    protected $context;

    /**
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        $this->context = $context;
    }

    /**
     * Returns custom save button
     *
     * @return array
     */
    public function getButtonData(): array
    {
        return [
            'label' => __('Save Post'),
            'class' => 'save primary',
            'on_click' => sprintf("location.href = '%s';", $this->getUrl()),
            'data_attribute' => [
                'form-role' => 'save',
            ],
            'sort_order' => 100,
        ];
    }

    /**
     * Returns URL for custom save button
     *
     * @return string
     */
    private function getUrl(): string
    {
        if ($id = (int)$this->context->getRequest()->getParam('id')) {
            return $this->context->getUrlBuilder()->getUrl(
                '*/*/save/',
                ['id' => $id]
            );
        }

        return $this->context->getUrlBuilder()->getUrl('*/*/save');
    }
}
