<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Blog\Block;

use Convert\Blog\Model\ResourceModel\Blog\CollectionFactory;
use Magento\Framework\View\Element\Template;

class View extends Template
{
    /** @var Template\Context */
    protected $context;

    /** @var CollectionFactory */
    protected $blogCollectionFactory;

    /**
     * View constructor.
     * @param Template\Context $context
     * @param CollectionFactory $blogCollectionFactory
     * @param array $data
     */
    public function __construct(Template\Context $context, CollectionFactory $blogCollectionFactory, array $data = [])
    {
        $this->context = $context;
        $this->blogCollectionFactory = $blogCollectionFactory;

        parent::__construct($context, $data);
    }

    /**
     * Returns collection of posts
     *
     * @return array
     */
    public function getPostCollection(): array
    {
        return $this->blogCollectionFactory->create()->getData();
    }
}
