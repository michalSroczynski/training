<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Blog\Controller\Adminhtml\Blog;

use Convert\Blog\Model\Blog;

use Convert\Blog\Model\ResourceModel\Blog as BlogResourceModel;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;

class Save extends Action implements HttpPostActionInterface
{
    /** @var PageFactory */
    protected $resultPageFactory;

    /** @var ForwardFactory */
    protected $resultForwardFactory;

    /** @var BlogResourceModel */
    protected $blogResourceModel;

    /** @var Blog */
    protected $blogModel;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * Save constructor.
     *
     * @param Context $context
     * @param ForwardFactory $resultForwardFactory
     * @param PageFactory $resultPageFactory
     * @param BlogResourceModel $blogResourceModel
     * @param Blog $blogModel
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        ForwardFactory $resultForwardFactory,
        PageFactory $resultPageFactory,
        BlogResourceModel $blogResourceModel,
        Blog $blogModel,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->resultForwardFactory = $resultForwardFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->blogResourceModel = $blogResourceModel;
        $this->blogModel = $blogModel;
        $this->logger = $logger;
    }

    /**
     * Execute save action
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $heading = $this->getRequest()->getParam('heading');
        $text = $this->getRequest()->getParam('text');
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($heading && $text) {
            try {
                $this->blogModel->setData(['heading' => $heading, 'text' => $text]);
                $this->blogResourceModel->save($this->blogModel);
                $this->messageManager->addSuccessMessage(__("Data Saved Successfully."));
            } catch (\Exception $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());
                $this->logger->error($exception->getMessage());
            }
        }

        $this->resultForwardFactory->create()->forward('index');
        return $resultRedirect;
    }
}
