<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Blog\Model\ResourceModel\Blog;

use Convert\Blog\Model\Blog;
use Convert\Blog\Model\ResourceModel\Blog as BlogResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Blog collection constructor
     */
    protected function _construct()
    {
        $this->_init(Blog::class, BlogResourceModel::class);
    }
}
