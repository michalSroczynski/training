<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Blog\Model;

use Magento\Framework\Model\AbstractModel;
use Convert\Blog\Model\ResourceModel\Blog as BlogResourceModel;

class Blog extends AbstractModel
{
    const CACHE_TAG = 'convert_blog';

    /**
     * Model constructor
     */
    public function _construct()
    {
        $this->_init(BlogResourceModel::class);
    }
}
