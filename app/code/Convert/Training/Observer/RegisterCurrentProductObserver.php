<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Training\Observer;

use Convert\Training\Registry\CurrentProduct;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class RegisterCurrentProductObserver implements ObserverInterface
{
    /** @var CurrentProduct */
    protected $currentProduct;

    /**
     * RegisterCurrentProductObserver constructor.
     * @param CurrentProduct $currentProduct
     */
    public function __construct(CurrentProduct $currentProduct)
    {
        $this->currentProduct = $currentProduct;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer): void
    {
        /** @var ProductInterface $product */
        $product = $observer->getData('product');
        $this->currentProduct->set($product);
    }
}
