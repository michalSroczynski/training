/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

let config = {
    map: {
        '*': {
            leavePage: 'Convert_Training/js/leavePage'
        }
    },
    config: {
        mixins: {
            'Convert_Training/js/leavePage': {
                'Convert_Training/js/clickLog': true
            }
        }
    }
};
