/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

define(
    [
        'jquery',
        'uiComponent',
        'ko'
    ], ($, component, ko) => {
        'use strict';

        return component.extend({
            url: ko.observable(''),
            initialize: function () {
                this._super();
                $("#show-url-button").on('click', function () {
                    $('#knockout-training').toggle();
                });

                this.url(this.getUrl());
            },

            getUrl: function () {
                let url = $('#leave-page-link').text().trim();

                if (typeof url !== 'undefined' && url) {
                    return url;
                }

                return '';
            }
        });
    }
);
