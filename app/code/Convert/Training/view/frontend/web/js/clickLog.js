/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */


define(
    [
        'jquery',
        'mage/utils/wrapper'
    ], function ($, wrapper) {
        'use strict';

        return () => {
            $("#leave-page-link").on('click', function () {
                console.log('Leave Page modal click.');
            });
        }
    });
