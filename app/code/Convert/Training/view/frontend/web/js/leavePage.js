/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

require(
    [
        'jquery',
        'Magento_Ui/js/modal/modal'
    ],
    function (
        $,
        modal
    ) {
        let initialize = () => {
            let popup = modal(getOptions(), $('#leave-page-modal'));
            $("#leave-page-link").on('click', function () {
                $("#leave-page-modal").modal("openModal");
            });
        }

        let getOptions = () => {
            return {
                type: 'popup',
                responsive: true,
                innerScroll: true,
                buttons: [{
                    text: $.mage.__('Continue'),
                    class: 'leave-page-modal-continue',
                    click: function () {
                        window.location.href = $('#leave-page-link').text().trim();
                    }
                }]
            };
        }

        return initialize();
    }

);


