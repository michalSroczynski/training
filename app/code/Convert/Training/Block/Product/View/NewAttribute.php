<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Training\Block\Product\View;

use Convert\Training\Registry\CurrentProduct;
use Magento\Framework\View\Element\Template;

class NewAttribute extends Template
{
    /** @var CurrentProduct */
    protected $currentProduct;

    /**
     * NewAttribute constructor.
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);

        /** @var CurrentProduct $currentProduct */
        $this->currentProduct = $this->getData('current_product');
    }

    /**
     * @return string
     */
    public function _toHtml(): string
    {
        if (!$this->getExternalUrl()) {
            return '';
        }

        return parent::_toHtml();
    }

    /**
     * @return string
     */
    public function getExternalUrl(): string
    {
        $attributeValue = $this->currentProduct->get()->getCustomAttribute($this->getData('new_attribute_code'));

        return $attributeValue ? $attributeValue->getValue() : '';
    }
}
