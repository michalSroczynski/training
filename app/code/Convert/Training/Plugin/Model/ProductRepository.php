<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Training\Plugin\Model;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Psr\Log\LoggerInterface;

class ProductRepository
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Save constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function beforeSave(ProductRepositoryInterface $productRepository, ProductInterface $product, $saveOptions = false): array
    {
        $this->logger->info("Training plugin - Before save product repository: " . $product->getName());

        return [$product];
    }

    /**
     * @param ProductRepositoryInterface $productRepository
     * @param \Closure $proceed
     * @param ProductInterface $product
     * @param bool $saveOptions
     * @return mixed
     */
    public function aroundSave(ProductRepositoryInterface $productRepository, \Closure $proceed, ProductInterface $product, $saveOptions = false)
    {
        $this->logger->info("Training plugin - Around save product repository: " . $product->getName());

        return $proceed($product, $saveOptions);
    }

    /**
     * @param ProductRepositoryInterface $product
     * @param $result
     * @return mixed
     */
    public function afterSave(ProductRepositoryInterface $product, $result)
    {
        $this->logger->info("Training plugin - After save product repository: " . $result->getName());

        return $result;
    }
}
