<?php
declare(strict_types=1);

/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Training\Plugin\Model;

use Magento\Catalog\Model\Product as ProductModel;
use Psr\Log\LoggerInterface;

class Product
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Save constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param ProductModel $product
     * @return ProductModel[]
     */
    public function beforeSave(ProductModel $product): array
    {
        $this->logger->info("Training plugin - Before save product: " . $product->getName());

        return [$product];
    }

    /**
     * @param ProductModel $product
     * @param callable $proceed
     * @return mixed
     */
    public function aroundSave(ProductModel $product, callable $proceed)
    {
        $this->logger->info("Training plugin - Around save product: " . $product->getName());
        return $proceed();
    }

    /**
     * @param ProductModel $product
     * @param $result
     * @return mixed
     */
    public function afterSave(ProductModel $product, $result)
    {
        $this->logger->info("Training plugin - After save product: " . $product->getName());

        return $result;
    }
}
