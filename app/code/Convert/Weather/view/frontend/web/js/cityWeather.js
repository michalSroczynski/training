/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

define([
    'jquery',
    'mage/translate'
], function (
    $,
    $t
) {
    let city = $t('City') + ': ';
    let temp = $t('Temp') + ': ';

    let getCurrentWeather = (url) => {
        $.ajax({
            url: url,
            type: 'GET',
            data: {
                format: 'json'
            },
            dataType: 'json',
            success: function (data) {
                let response = JSON.parse(data);
                let currentCity = response.city;
                let currentTemp = response.temp;
                $('#city-weather').html(
                    city + currentCity +
                    ' ' +
                    temp + currentTemp
                )
            },
            error: function () {
                $('#city-weather').html('--')
            }
        });
    }

    return function (config) {
        getCurrentWeather(config.url);
    }
});
