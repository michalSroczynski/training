/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

let config = {
    map: {
        '*': {
            weather: 'Convert_Weather/js/cityWeather'
        }
    }
};
