<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Weather\Api;

interface WeatherInterface
{
    /**
     * @return string
     */
    public function getWeather(): string;
}
