<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Weather\Model;

use Convert\Weather\Api\WeatherInterface;
use Convert\Weather\Helper\Data;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Serialize\Serializer\Json;
use Psr\Log\LoggerInterface;

class Weather implements WeatherInterface
{
    /** @var RequestInterface */
    protected $request;

    /** @var Curl */
    protected $curl;

    /** @var Data */
    protected $data;

    /** @var Json */
    protected $jsonSerializer;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * Weather constructor.
     * @param RequestInterface $request
     * @param Curl $curl
     * @param Data $data
     * @param Json $jsonSerializer
     * @param LoggerInterface $logger
     */
    public function __construct(
        RequestInterface $request,
        Curl $curl,
        Data $data,
        Json $jsonSerializer,
        LoggerInterface $logger
    ) {
        $this->request = $request;
        $this->curl = $curl;
        $this->data = $data;
        $this->jsonSerializer = $jsonSerializer;
        $this->logger = $logger;
    }

    /**
     * @return string
     */
    public function getWeather(): string
    {
        $currentWeather = $this->getCurrentWeather();
        $currentTemp = $this->getCurrentTemp($currentWeather);
        $currentCity = $this->getCurrentCity($currentWeather);

        return $this->jsonSerializer->serialize([
            'city' => $currentCity,
            'temp' => $currentTemp
        ]);
    }

    /**
     * @param array $currentWeather
     * @return string
     */
    public function getCurrentTemp(array $currentWeather): string
    {
        $result = '---';

        try {
            $result = (string)$currentWeather['current']['temp_c'];
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        return $result;
    }

    /**
     * @param $currentWeather
     * @return string
     */
    public function getCurrentCity(array $currentWeather): string
    {
        $result = '---';

        try {
            $result = $currentWeather['location']['name'];
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        return $result;
    }

    /**
     * @return array
     */
    private function getCurrentWeather(): array
    {
        $apiKey = $this->data->getApiKey();
        $apiUrl = $this->data->getApiUrl();
        $currentCity = $this->data->getCurrentCity();

        try {
            $this->curl->get(
                $apiUrl .
                '?' .
                http_build_query(['q' => $currentCity, 'key' => $apiKey])
            );

            return $this->jsonSerializer->unserialize($this->curl->getBody());
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        return [];
    }
}
