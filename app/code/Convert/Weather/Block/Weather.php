<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Weather\Block;

use Convert\Weather\Helper\Data;
use Magento\Framework\View\Element\Template;

class Weather extends Template
{
    /** @var Data */
    protected $dataHelper;

    /**
     * NewAttribute constructor.
     * @param Template\Context $context
     * @param Data $dataHelper
     * @param array $data
     */
    public function __construct(Template\Context $context, Data $dataHelper, array $data = [])
    {
        parent::__construct($context, $data);

        $this->dataHelper = $dataHelper;
    }

    /**
     * @return string
     */
    public function getCurrentCity(): string
    {
        return $this->dataHelper->getCurrentCity();
    }

    /**
     * @return string
     */
    public function _toHtml(): string
    {
        if (!$this->dataHelper->isWeatherEnabled()) {
            return '';
        }

        return parent::_toHtml();
    }
}
