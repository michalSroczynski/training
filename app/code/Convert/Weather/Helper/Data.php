<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Weather\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    const WEATHER_CURRENT_CITY_PATH = 'convert-weather/general/city';
    const WEATHER_API_KEY_PATH = 'convert-weather/general/key';
    const WEATHER_API_URL_PATH = 'convert-weather/general/url';
    const WEATHER_API_ENABLED = 'convert-weather/general/enable';

    /**
     * @return string
     */
    public function getCurrentCity(): string
    {
        return $this->scopeConfig->getValue(self::WEATHER_CURRENT_CITY_PATH);
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->scopeConfig->getValue(self::WEATHER_API_KEY_PATH);
    }

    /**
     * @return string
     */
    public function getApiUrl(): string
    {
        return $this->scopeConfig->getValue(self::WEATHER_API_URL_PATH);
    }

    /**
     * @return bool
     */
    public function isWeatherEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(self::WEATHER_API_ENABLED);
    }
}
