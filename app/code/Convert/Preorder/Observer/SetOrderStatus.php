<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Preorder\Observer;

use Convert\Preorder\Helper\Data;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order\Item;
use Magento\Sales\Model\Order\Payment;

class SetOrderStatus implements ObserverInterface
{
    /** @var Data */
    protected $dataHelper;

    public function __construct(Data $dataHelper)
    {
        $this->dataHelper = $dataHelper;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer): void
    {
        /** @var Payment $payment */
        $payment = $observer->getData('payment');

        /** @var Item $item */
        foreach ($payment->getOrder()->getItems() as $item) {
            if ($item->getProduct()->getAllowPreorder()) {
                $payment->getOrder()->setStatus(Data::PREORDER_REVIEW_ORDER_STATUS);
                return;
            }
        }
    }
}
