<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Preorder\Controller\Preorder;

use Convert\Preorder\Exception\OrderNotFoundException;
use Convert\Preorder\Helper\Data;
use Exception;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\QuoteManagement;
use Magento\Quote\Model\QuoteRepository;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Sales\Model\OrderRepository;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use \Magento\Quote\Api\Data\AddressInterface;
use \Magento\Quote\Api\Data\AddressInterfaceFactory;
use \Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\CartRepositoryInterface;

class BuyNow implements HttpGetActionInterface
{
    /** @var Context */
    protected $context;

    /** @var ProductRepositoryInterface */
    protected $productRepository;

    /** @var QuoteManagement */
    protected $quoteManagement;

    /** @var OrderSender */
    protected $orderSender;

    /** @var Session */
    protected $customer;

    /** @var RedirectFactory */
    protected $redirectFactory;

    /** @var StoreManagerInterface */
    protected $storeManager;

    /** @var LoggerInterface */
    protected $logger;

    /** @var CartManagementInterface */
    protected $cartManagement;

    /** @var AddressInterfaceFactory */
    protected $addressFactory;

    /** @var CartRepositoryInterface */
    protected $cartRepository;

    /** @var QuoteRepository */
    protected $quoteRepository;

    protected $orderRepository;

    /**
     * @param Context $context
     * @param ProductRepositoryInterface $productRepository
     * @param QuoteManagement $quoteManagement
     * @param QuoteRepository $quoteRepository
     * @param OrderSender $orderSender
     * @param Session $customer
     * @param RedirectFactory $redirectFactory
     * @param StoreManagerInterface $storeManager
     * @param LoggerInterface $logger
     * @param CartManagementInterface $cart
     * @param AddressInterfaceFactory $addressFactory
     * @param CartRepositoryInterface $cartRepository
     */
    public function __construct(
        Context $context,
        ProductRepositoryInterface $productRepository,
        QuoteManagement $quoteManagement,
        QuoteRepository $quoteRepository,
        OrderSender $orderSender,
        Session $customer,
        RedirectFactory $redirectFactory,
        StoreManagerInterface $storeManager,
        LoggerInterface $logger,
        CartManagementInterface $cart,
        AddressInterfaceFactory $addressFactory,
        CartRepositoryInterface $cartRepository,
        OrderRepository $orderRepository
    ) {
        $this->context = $context;
        $this->productRepository = $productRepository;
        $this->quoteManagement = $quoteManagement;
        $this->orderSender = $orderSender;
        $this->customer = $customer;
        $this->redirectFactory = $redirectFactory;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
        $this->cartManagement = $cart;
        $this->addressFactory = $addressFactory;
        $this->cartRepository = $cartRepository;
        $this->quoteRepository = $quoteRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     */
    public function execute()
    {
        try {
            $productId = $this->context->getRequest()->getParam('product_id');
            $qty = $this->context->getRequest()->getParam('qty');

            $address = $this->getAddress();
            $orderId = $this->placeOrder($productId, $qty, $address);

            $order = $this->orderRepository->get($orderId);

            if (!$orderIncrementId = $order->getIncrementId()) {
                throw new OrderNotFoundException(__('Error while creating order.'));
            }

            $this->context->getMessageManager()
                ->addSuccessMessage(__("New Order (%1) Created Successfully.", $orderIncrementId));
        } catch (Exception $exception) {
            $this->context->getMessageManager()
                ->addErrorMessage($exception->getMessage());
            $this->logger->error($exception->getMessage());
        }

        $resultRedirect = $this->redirectFactory->create();
        $resultRedirect->setRefererUrl();
        return $resultRedirect;
    }

    /**
     * @return AddressInterface
     */
    protected function getAddress(): AddressInterface
    {
        $customerData = $this->customer->getCustomerDataObject();
        $customerBillingAddress = $customerData->getAddresses()[0];

        return $this->addressFactory->create(
            [
                'data' => [
                    AddressInterface::KEY_COUNTRY_ID => $customerBillingAddress->getCountryId(),
                    AddressInterface::KEY_REGION_ID => $customerBillingAddress->getRegionId(),
                    AddressInterface::KEY_LASTNAME => $customerBillingAddress->getLastname(),
                    AddressInterface::KEY_FIRSTNAME => $customerBillingAddress->getFirstname(),
                    AddressInterface::KEY_STREET => $customerBillingAddress->getStreet(),
                    AddressInterface::KEY_EMAIL => $customerData->getEmail(),
                    AddressInterface::KEY_CITY => $customerBillingAddress->getCity(),
                    AddressInterface::KEY_TELEPHONE => $customerBillingAddress->getTelephone(),
                    AddressInterface::KEY_POSTCODE => $customerBillingAddress->getPostcode(),
                    AddressInterface::KEY_PREFIX => $customerBillingAddress->getPrefix(),
                    AddressInterface::KEY_SUFFIX => $customerBillingAddress->getSuffix(),
                    AddressInterface::SAVE_IN_ADDRESS_BOOK => 0
                ]
            ]
        );
    }

    /**
     * @param string $productId
     * @param string $qty
     * @param AddressInterface $address
     * @return int
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws CouldNotSaveException
     */
    protected function placeOrder(string $productId, string $qty, AddressInterface $address): int
    {
        /** @var Store $store */
        $store = $this->storeManager->getStore();
        $customerData = $this->customer->getCustomerDataObject();

        $cartId = $this->cartManagement->createEmptyCart();
        $cart = $this->cartRepository->get($cartId);
        $cart->setCustomerEmail($customerData->getEmail())
            ->setCustomerIsGuest(false)
            ->assignCustomer($customerData)
            ->setStoreId($store->getId())
            ->setBillingAddress($address)
            ->setShippingAddress($address)
            ->assignCustomer($customerData);

        $product = $this->productRepository->getById($productId);
        $cart->addProduct($product, (int)$qty);
        $cart->getPayment()->importData(['method' => Data::PREORDER_PAYMENT_METHOD_NAME]);
        $cart->getShippingAddress()->setShippingMethod(Data::FREE_SHIPPING_METHOD_NAME);
        $this->cartRepository->save($cart);
        $orderId = $this->quoteManagement->placeOrder($cartId);
        $cart->setIsActive(false);

        return (int)$orderId;
    }
}
