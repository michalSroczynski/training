<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Preorder\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    const PREORDER_ENABLED = 'payment/newpayment/enable';
    const PREORDER_REVIEW_ORDER_STATUS = 'preorder_review';
    const PREORDER_PAYMENT_METHOD_NAME = 'newpayment';
    const FREE_SHIPPING_METHOD_NAME = 'freeshipping_freeshipping';

    /**
     * @return bool
     */
    public function isPreorderEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(self::PREORDER_ENABLED);
    }
}
