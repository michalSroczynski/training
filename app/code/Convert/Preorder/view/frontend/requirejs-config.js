/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

let config = {
    map: {
        '*': {
            preorder: 'Convert_Preorder/js/buyNow'
        }
    }
};
