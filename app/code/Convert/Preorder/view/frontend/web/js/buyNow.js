/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

define(
    [
        'jquery',
        'mage/utils/wrapper'
    ],
    function ($, wrapper) {
        'use strict';

        let buyNowRedirect = (url) => {
            $("#buy-now").on('click', function () {
                let qty = $('#qty').val();
                document.location.href = url + 'qty/' + qty;
            });
        }

        return function (config) {
            buyNowRedirect(config.url);
        }
    }
);
