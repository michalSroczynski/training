<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Preorder\Exception;

use Magento\Framework\Exception\NotFoundException;

class OrderNotFoundException extends NotFoundException
{
}
