<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Preorder\Block\Product\View;

use Convert\Preorder\Helper\Data;
use Convert\Training\Registry\CurrentProduct;
use Magento\Customer\Model\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Framework\App\Http\Context as AuthContext;

class PlaceOrder extends Template
{
    /** @var CurrentProduct */
    protected $currentProduct;

    /** @var StoreInterface */
    protected $store;

    /** @var Data */
    protected $dataHelper;

    /** @var AuthContext */
    protected $authContext;

    /**
     * NewAttribute constructor.
     * @param Template\Context $context
     * @param Data $dataHelper
     * @param AuthContext $authContext
     * @param array $data
     * @throws NoSuchEntityException
     */
    public function __construct(
        Template\Context $context,
        Data $dataHelper,
        AuthContext $authContext,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->currentProduct = $this->getData('product');
        $this->store = $context->getStoreManager()->getStore();
        $this->dataHelper = $dataHelper;
        $this->authContext = $authContext;
    }

    /**
     * @return int
     */
    public function getCurrentProductId(): int
    {
        return (int)$this->currentProduct->get()->getId();
    }

    /**
     * @return string
     */
    public function _toHtml(): string
    {
        if (!$this->dataHelper->isPreorderEnabled() || !$this->authContext->getValue(Context::CONTEXT_AUTH)) {
            return '';
        }

        return parent::_toHtml();
    }
}
