<?php
declare(strict_types=1);
/**
 * @author Convert Team
 * @copyright Copyright (c) Convert (https://www.convert.no/)
 */

namespace Convert\Preorder\Model;

use Magento\Payment\Model\Method\AbstractMethod;

class Newpayment extends AbstractMethod
{

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'newpayment';

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;

    protected $_isInitializeNeeded = true;
}
